<?php 
class M_mapping_notification extends M_myweb
{
	public function get_mapping_notification($squad, $project)
    {
        $arr= array();
        $this->db->where('mapping_notification.project_id',$project);
        $this->db->where('projects.squad_id',$squad);
        $this->db->select('mapping_notification.id,mapping_notification.tag_commit,mapping_notification.dev_commit,mapping_notification.pipeline_id,mapping_notification.tag_name,mapping_notification.create_time,projects.project_name,projects.git_name, squads.squad_name');
        $this->db->from('mapping_notification');
        $this->db->join('projects', 'mapping_notification.project_id = projects.id');
        $this->db->join('squads', 'projects.squad_id = squads.id');
        $query = $this->db->get();
        foreach($query->result() as $row)
		{   
            $arr[]=$row;
		}
		return $arr;
    }
}
?>