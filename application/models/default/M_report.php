<?php 
class M_report extends CI_model
{
    public function get_reports($squad, $project)
    {
        $arr= array();
        $this->db->where('reports.project_id',$project);
        $this->db->where('projects.squad_id',$squad);
        $this->db->where('reports.status',1);
        $this->db->select('reports.id,reports.file_name,reports.create_time,projects.project_name, squads.squad_name');
        $this->db->from('reports');
        $this->db->join('projects', 'reports.project_id = projects.id');
        $this->db->join('squads', 'projects.squad_id = squads.id');
        $query = $this->db->get();
        foreach($query->result() as $row)
		{   
            $arr[]=$row;
		}
		return $arr;
    }

    public function delete_report($id)
    {
        $arr= array();
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update('reports');
    }

    public function insert_report($report_name, $project_name)
    {
        $project_id = $this->get_project_id($project_name);
        $time=time();
        $data = array(
            'file_name' => $report_name,
            'project_id' => $project_id,
            'create_time' => $time,   
        );
        $this->db->insert('reports', $data);
    }

    public function get_project_id($name)
    {
        $arr= array();
        $this->db->where('project_name',$name);
        $project = $this->db->get('projects')->row();
		return $project->id;
    }
    
}
?>