<?php 
class M_main extends CI_model
{
    public function get_reports()
    {
        $arr= array();
        $this->db->where('reports.status',1);
        $this->db->from('reports');
        $this->db->join('projects', 'reports.project_id = projects.id');
        $this->db->join('squads', 'projects.squad_id = squads.id');
        $query = $this->db->get();
        foreach($query->result() as $row)
		{   
            $arr[]=$row;
		}
		return $arr;
    }

    public function get_squads(){
        $arr= array();
        $this->db->where('status',1);
        $query = $this->db->get("squads");
        foreach($query->result() as $row)
		{   
            $row->projects = $this->get_project($row->id);
            $arr[]=$row;
		}
		return $arr;
    }

    public function get_project($squad){
        $arr= array();
        $this->db->where('status',1);
        $this->db->where('squad_id',$squad);
        $query = $this->db->get("projects");
        foreach($query->result() as $row)
		{   
            $arr[]=$row;
		}
		return $arr;
    }
}
?>