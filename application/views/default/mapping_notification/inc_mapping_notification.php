<div class="row">
    <div class="col-12">
        <div class="card my-4">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
            <h6 class="text-white text-capitalize ps-3">MAPPING NOTIFICATION FOR SQUAD</h6>
            </div>
        </div>
        <div class="card-body px-0 pb-2">
            <div class="table-responsive p-0">
            <table class="table align-items-center mb-0">
                <thead>
                <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pipeline Id</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Dev commit</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Tag commit</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Tag name</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Project</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Time</th>
                    <th class="text-secondary opacity-7"></th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach($mapping_notifications as $item):?>
                    
                        <tr>
                            <td>
                            <div class="d-flex px-2 py-1">
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm"><?php echo $item->pipeline_id ?></h6>
                            </div>
                            </div>
                            </td>
                            <td>
                                <p class="text-xs font-weight-bold mb-0"><?php echo $item->dev_commit ?></p>
                            </td>
                            <td>
                                <p class="text-xs font-weight-bold mb-0"><?php echo $item->tag_commit ?></p>
                            </td>
                            <td>
                                <span class="badge badge-sm bg-gradient-success"><?php echo $item->tag_name ?></span>
                            </td>
                            <td class="align-middle text-center text-sm">
                                <span class="badge badge-sm bg-gradient-success"><?php echo $item->project_name ?></span>
                            </td>
                            <td class="align-middle text-center">
                                <span class="text-secondary text-xs font-weight-bold"><?php echo date('m/d/Y H:i:s', $item->create_time);?></span>
                            </td>
                            <td class="align-middle">
                                <a href="https://git.vndev.shopee.io/qc/<?php echo $item->git_name; ?>/-/pipelines/<?php echo $item->pipeline_id ?>" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                                    View
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>
</div>