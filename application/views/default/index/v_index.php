<body class="g-sidenav-show  bg-gray-200">
    <?php $this->load->view('default/_layout/menu');?>

  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <div class="container-fluid py-4">
        <?php include 'inc_report_list.php'; ?>
    </div>
  </main>
