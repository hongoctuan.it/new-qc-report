<div class="row">
    <div class="col-12">
        <div class="card my-4">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
            <h6 class="text-white text-capitalize ps-3">REPORT LIST FOR SQUAD</h6>
            </div>
        </div>
        <div class="card-body px-0 pb-2">
            <div class="table-responsive p-0">
            <table class="table align-items-center mb-0">
                <thead>
                <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reports name</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Project</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Squad</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Time</th>
                    <th class="text-secondary opacity-7"></th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach($reports as $item):?>
                    
                        <tr>
                            <td>
                            <div class="d-flex px-2 py-1">
                                <div>
                                <img src="<?php echo base_url(); ?>assets/img/team-2.jpg" class="avatar avatar-sm me-3 border-radius-lg" alt="user1">
                                </div>
                                <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm"><?php echo $item->file_name ?></h6>
                                </div>
                            </div>
                            </td>
                            <td>
                            <p class="text-xs font-weight-bold mb-0"><?php echo $item->project_name ?></p>
                            </td>
                            <td class="align-middle text-center text-sm">
                            <span class="badge badge-sm bg-gradient-success"><?php echo $item->squad_name ?></span>
                            </td>
                            <td class="align-middle text-center">
                            <span class="text-secondary text-xs font-weight-bold"><?php echo date('m/d/Y H:i:s', $item->create_time);?></span>
                            </td>
                            <td class="align-middle">
                                <a href="<?php echo base_url()."report-detail/".$item->file_name ?>" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                                    View
                                </a>
                                &nbsp | &nbsp
                                <a href="<?php echo base_url()."report-delete/".$item->id; ?>" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                                    Delete
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>
</div>