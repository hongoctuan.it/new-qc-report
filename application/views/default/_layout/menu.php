<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href=" https://demos.creative-tim.com/material-dashboard/pages/dashboard " target="_blank">
        <img src="<?php echo base_url(); ?>assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">QC Report Dashboard</span>
      </a>
    </div>
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
    <?php foreach($squads as $squad):?>
      <ul class="navbar-nav">
        <hr class="horizontal light mt-0">
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#pagesExamples" class="nav-link text-white" aria-controls="pagesExamples" role="button" aria-expanded="true">
            <i class="material-icons-round {% if page.brand == 'RTL' %}ms-2{% else %} me-2{% endif %}">image</i>
            <span class="nav-link-text ms-2 ps-1"><?php echo $squad->squad_name;?></span>
          </a>
          <div class="collapse show" id="pagesExamples" style="">
            <ul class="nav ">
              <?php foreach($squad->projects as $project):?>
                <li class="nav-item ">
                  <a class="nav-link text-white collapsed" data-bs-toggle="collapse" aria-expanded="false" href="#profileExample">
                    <span class="sidenav-mini-icon"> P </span>
                    <span class="sidenav-normal  ms-2  ps-1"> <?php echo $project->project_name;?> <b class="caret"></b></span>
                  </a>
                  <div class="collapse" id="profileExample" style="">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a class="nav-link text-white " href="<?php echo base_url().'report-list/'.$squad->id.'/'.$project->id; ?>">
                          <span class="sidenav-mini-icon"> R </span>
                          <span class="sidenav-normal  ms-2  ps-1"> Report </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link text-white " href="<?php echo base_url().'mapping-notification/'.$squad->id.'/'.$project->id; ?>">
                          <span class="sidenav-mini-icon"> P </span>
                          <span class="sidenav-normal  ms-2  ps-1"> Maping Notification </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
                <!-- <li class="nav-item ">
                  <a class="nav-link text-white " href="../../pages/pages/pricing-page.html">
                    <span class="sidenav-mini-icon"> P </span>
                    <span class="sidenav-normal  ms-2  ps-1"> Pricing Page </span>
                  </a>
                </li> -->
              <?php endforeach;?>
            </ul>
          </div>
        </li>
      </ul>
    <?php endforeach;?>
    </div>
  </aside>