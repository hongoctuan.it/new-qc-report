<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_list extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('default/m_report');
		$this->load->model('default/m_main');
		$this->data['squads']  = $this->m_main->get_squads();
	}
	public function index($squad, $project)
	{
		$this->data['reports']  = $this->m_report->get_reports($squad,$project);
		$_SESSION['squad'] = $squad;
		$_SESSION['project'] = $project;
		$this->data['title']	= "Trang Chủ";
		$this->data['subview'] 	= 'default/index/v_index';
		$this->load->view('default/_main_page', $this->data);
	}

	public function editreport()
	{
		$json = file_get_contents('php://input');
    	// Converts it into a PHP object
		$jsoncontent = json_decode($json);
		print_r($jsoncontent);
		if($jsoncontent->type=='html'){
			$file = str_replace('data:image/png;base64,','', $jsoncontent->reportfile);
			$file = str_replace(' ', '+', $file);
			$data = base64_decode($file);
			$success = file_put_contents("reports/".$jsoncontent->namefile, $data);
		}else{
			$file = str_replace('data:image/png;base64,', '', $jsoncontent->reportfile);
			$file = str_replace(' ', '+', $file);
			$data = base64_decode($file);
			$success = file_put_contents("screenshots/".$jsoncontent->namefile, $data);
			$file = @fopen('reports/'.$jsoncontent->reportname, 'r');
			if($file === false){
				echo 'Error when reading the file';
				exit;  
			}
			//reading line by line
			$output ='';
			while (($line = fgets($file)) !== FALSE) {
				if(strpos($line,"pytest_screenshots")==TRUE){
					$line_arr = explode(' ',$line);
					for($i = 0; $i< count($line_arr);$i++){
						if(strpos($line_arr[$i],"pytest_screenshots")==TRUE){
							$temp = $line_arr[$i];
							$temp_arr = explode('/',$temp);
							$url = $temp_arr[0]."../../../screenshots/".$temp_arr[count($temp_arr)-1];
							$line_arr[$i]=$url;
						}
					}
					$output .= implode(' ',$line_arr);
				}
				else{
					$output .= $line;
				}
			}
			fclose($file);
			$result = file_put_contents("reports/".$jsoncontent->namefile, $output);
		}
		$this->m_report->insert_report($jsoncontent->namefile,$jsoncontent->project);
	}

	public function delete_report($id)
	{
		$this->m_report->delete_report($id);
		Redirect(base_url().'report-list/'.$_SESSION['squad'].'/'.$_SESSION['project']);
	}
}
