<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('default/m_main');
		$this->data['squads']  = $this->m_main->get_squads();
	}
	public function index()
	{
		$this->data['reports']  = $this->m_main->get_reports();
		$this->data['title']	= "Trang Chủ";
		$this->data['subview'] 	= 'default/index/v_index';
		$this->load->view('default/_main_page', $this->data);
	}
}
