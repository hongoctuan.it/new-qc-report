<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mapping_notification extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('default/m_mapping_notification');
		$this->load->model('default/m_main');
		$this->data['squads']  = $this->m_main->get_squads();
	}
	public function index($squad, $project)
	{
		$this->data['mapping_notifications']  = $this->m_mapping_notification->get_mapping_notification($squad,$project);
		$_SESSION['squad'] = $squad;
		$_SESSION['project'] = $project;
		$this->data['title']	= "Mapping-Notification";
		$this->data['subview'] 	= 'default/mapping_notification/v_index';
		$this->load->view('default/_main_page', $this->data);	
	}

	
}
