<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_detail extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	public function index($file_name)
	{
		Redirect(base_url()."reports/".$file_name, false);
	}


}
